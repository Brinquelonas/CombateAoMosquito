﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[System.Serializable]
public class PotaTweenAnimationTween
{

    public void Play(System.Action callback = null)
    {

    }
}

[System.Serializable]
public class PotaTweenAnimation
{
    public string Name;
    public List<PotaTweenAnimationTween> Tweens;

    public void Play(System.Action callback = null)
    {
        for (int i = 0; i < Tweens.Count; i++)
        {
            Tweens[i].Play(callback);
        }
    }
}

public class PotaTweenAnimationController : MonoBehaviour
{
    public List<PotaTweenAnimation> Animations;

    public void Play(string animationName)
    {
        Play(animationName, null);
    }

    public void Play(string animationName, System.Action callback)
    {

    }
}
