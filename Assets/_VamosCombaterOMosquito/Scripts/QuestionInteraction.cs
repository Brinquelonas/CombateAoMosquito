﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using Spine.Unity;
using TMPro;

public class QuestionInteraction : MonoBehaviour {

    [System.Serializable]
    public struct InteractionConfig
    {
        public string Tag;
        public Interaction Interaction;
    }

	public Image Balloon;
	public TextMeshProUGUI Question;
	public Button[] Buttons;
	public Button[] ImageButtons;
    public List<NPCController> NPCs = new List<NPCController>();
    public List<InteractionConfig> Interactions = new List<InteractionConfig>();
    public string ResourcesPath = "";

	//private bool _found = false;
    private Question _currentQuestion;

    public int Space { get; set; }
    public int CorrectAnswer { get; set; }

    private NPCController CurrentNPC
    {
        get
        {
            return NPCs[_currentQuestion.NPCIndex - 1];
        }
    }

	protected void Awake()
	{
		for (int i = 0; i < Buttons.Length; i++) 
		{
			InitializeButton (i);
		}
        for (int i = 0; i < ImageButtons.Length; i++)
        {
            InitializeImageButton(i);
        }
    }

    private void Update()
    {
        if (Input.GetKeyDown(KeyCode.LeftControl))
        {
            OnHandlerFound(0);
        }
    }

	void InitializeButton(int index)
	{
		Buttons[index].onClick.AddListener(() => Answer (index));
	}

    void InitializeImageButton(int index)
    {
        ImageButtons[index].onClick.AddListener(() => Answer(index));
    }

    void EnableButtons()
	{
        for (int i = 0; i < Buttons.Length; i++)
            Buttons[i].enabled = true;
        for (int i = 0; i < ImageButtons.Length; i++)
            ImageButtons[i].enabled = true;
    }

    void DisableButtons()
	{
		for (int i = 0; i < Buttons.Length; i++) 
			Buttons [i].enabled = false;
        for (int i = 0; i < ImageButtons.Length; i++)
            ImageButtons[i].enabled = false;
    }

    public void OnHandlerFound(int space)
	{
        //if (_found)
        //	return;
        //
        //GetComponent<Canvas>().enabled = true;
        //
        //_found = true;

        if (gameObject.activeSelf)
            return;

        gameObject.SetActive(true);

		EnableButtons ();

        Space = space;
        CorrectAnswer = Random.Range(0, 3);

        List<Question> questions = QuestionsTSVReader.Instance.GetQuestionsFromSpace(Space);
        int questionIndex = Random.Range(0, questions.Count);
        _currentQuestion = questions[questionIndex];

        Question.text = _currentQuestion.Text;
        Balloon.gameObject.SetActive(false);

        CurrentNPC.gameObject.SetActive(true);
        PlaySpineAnimation ("waiting", true);


        switch (_currentQuestion.Type)
        {
            case QuestionType.None:
                break;
            case QuestionType.TXT:
                InitializeTXTQuestion();
                break;
            case QuestionType.IMG:
                InitializeIMGQuestion();
                break;
            case QuestionType.INT:
                InitializeINTQuestion();
                break;
            default:
                break;
        }

        PotaTween.Create(CurrentNPC.gameObject).Stop();
        PotaTween.Create(CurrentNPC.gameObject).
            SetAlpha(0f, 1f).
            SetDuration(0.5f).
            Play(() => 
            {
                PlaySpineAnimation("question", false , () =>
                {
                    PlaySpineAnimation("waiting", true);
                });
                ShowBalloon(() =>
                {
                    PlayAudio(_currentQuestion.Audio, () => 
                    {
                        StartCoroutine(ShowAnswers());
                    });
                });
            });
	}

    public void Answer(int answer)
	{
		DisableButtons();

		System.Action callback = () => 
		{
            HideAll();
		};

		if (answer == CorrectAnswer)
        {
            PlaySpineAnimation("right", false, callback);
            PlayAudio(null);
            SapceText result = QuestionsTSVReader.Instance.GetSpaceText(Space, SpaceTextType.Positive);
            Question.text = "Parabéns!\n" + result.Text;
        }
		else
        {
			PlaySpineAnimation("wrong", false, callback);
            PlayAudio(null);
            SapceText result = QuestionsTSVReader.Instance.GetSpaceText(Space, SpaceTextType.Negative);
            Question.text = "Que pena...\n" + result.Text;
        }

        StartCoroutine(HideWrongAnswers());
	}

    private void InitializeTXTQuestion()
    {
        List<string> wrongAnswers = new List<string>(_currentQuestion.WrongAnswers);
        for (int i = 0; i < Buttons.Length; i++)
        {
            if (i == CorrectAnswer)
            {
                Buttons[i].GetComponentInChildren<TextMeshProUGUI>().text = _currentQuestion.RightAnswer;
            }
            else
            {
                int answerIndex = Random.Range(0, wrongAnswers.Count);
                Buttons[i].GetComponentInChildren<TextMeshProUGUI>().text = wrongAnswers[answerIndex];
                wrongAnswers.RemoveAt(answerIndex);
            }
            Buttons[i].gameObject.SetActive(false);
        }
    }

    private void InitializeIMGQuestion()
    {
        List<string> wrongAnswers = new List<string>(_currentQuestion.WrongAnswers);
        for (int i = 0; i < ImageButtons.Length; i++)
        {
            string txt = "";
            if (i == CorrectAnswer)
            {
                txt = _currentQuestion.RightAnswer;
            }
            else
            {
                int answerIndex = Random.Range(0, wrongAnswers.Count);
                txt = wrongAnswers[answerIndex];
                wrongAnswers.RemoveAt(answerIndex);
            }
            Image img = ImageButtons[i].transform.FindChild("Image").GetComponent<Image>();
            img.sprite = Resources.Load<Sprite>(ResourcesPath + "/" + txt);
            img.SetNativeSize();
            ImageButtons[i].gameObject.SetActive(false);
        }
    }

    private void InitializeINTQuestion()
    {
        for (int i = 0; i < Interactions.Count; i++)
        {
            if (Interactions[i].Tag == _currentQuestion.RightAnswer)
            {
                Interactions[i].Interaction.gameObject.SetActive(true);
                Interactions[i].Interaction.StartInteraction(Space);
                Interactions[i].Interaction.OnFinish.AddListener((b) =>
                {
                    if (b)
                        Answer(CorrectAnswer);
                    else
                        Answer(-1);
                    Interactions[i].Interaction.OnFinish.RemoveAllListeners();
                });

                PotaTween.Create(Interactions[i].Interaction.gameObject).
                    SetAlpha(0f, 1f).
                    SetDuration(0.5f).
                    Play();

                break;
            }
        }
    }

    private void PlayAudio(AudioClip clip, System.Action callback = null)
    {
        if (clip == null)
        {
            if (callback != null) callback();
            CurrentNPC.PlayMouthAnimation(2, () => CurrentNPC.PlayMouthAnimation(CurrentNPC.CurrentAnimationState));
            return;
        }

        AudioSource.PlayClipAtPoint(clip, Vector3.zero);

        if (callback != null)
            StartCoroutine(AudioCallback(clip, callback));
    }

    private void PlaySpineAnimation(string animationName, bool loop, System.Action callback = null)
    {
        CurrentNPC.PlayAnimation(animationName, loop, callback);
    }

    private void ShowBalloon(System.Action callback = null)
    {
        Balloon.gameObject.SetActive(true);

        PotaTween.Create(Balloon.gameObject).Stop();
        PotaTween.Create(Balloon.gameObject).
            SetAlpha(0f, 1f).
            SetScale( new Vector3(0.8f, 0.8f, 1), Vector3.one).
            SetDuration(0.5f).
            SetEaseEquation(Ease.Equation.OutBack).
            Play(callback);
    }

    private void HideButton(int i)
    {
        PotaTween.Create(Balloon.gameObject).Stop();
        if (_currentQuestion.Type == QuestionType.TXT)
        {
            PotaTween.Create(Buttons[i].gameObject).
                SetAlpha(1f, 0f).
                SetScale(Vector3.one, new Vector3(0.8f, 0.8f, 1)).
                SetDuration(0.5f).
                SetEaseEquation(Ease.Equation.OutSine).
                Play(() => 
                {
                    Buttons[i].gameObject.SetActive(false);
                });
        }
        else if (_currentQuestion.Type == QuestionType.IMG)
        {
            PotaTween.Create(ImageButtons[i].gameObject).
                SetAlpha(1f, 0f).
                SetScale(Vector3.one, new Vector3(0.8f, 0.8f, 1)).
                SetDuration(0.5f).
                SetEaseEquation(Ease.Equation.OutSine).
                Play(() =>
                {
                    Buttons[i].gameObject.SetActive(false);
                });
        }
    }

    private void HideAll()
    {
        HideElement(Balloon.gameObject);
        if (_currentQuestion.Type == QuestionType.TXT)
        {
            HideElement(Buttons[CorrectAnswer].gameObject, () =>
            {
            });
        }
        else if (_currentQuestion.Type == QuestionType.IMG)
        {
            HideElement(ImageButtons[CorrectAnswer].gameObject, () =>
            {
            });
        }
        else
        {
            for (int i = 0; i < Interactions.Count; i++)
            {
                if (Interactions[i].Interaction.gameObject.activeSelf)
                {
                    Interaction interaction = Interactions[i].Interaction;
                    DisableInteraction(interaction);
                }
            }
        }
        HideElement(CurrentNPC.gameObject, () => 
        {
            CurrentNPC.gameObject.SetActive(false);
            //GetComponent<Canvas>().enabled = false;
            //_found = false;
            gameObject.SetActive(false);
        });
    }

    private void DisableInteraction(Interaction interaction)
    {
        HideElement(interaction.gameObject, () =>
        {
            interaction.gameObject.SetActive(false);
        });
    }

    private void HideElement(GameObject element, System.Action callback = null)
    {
        PotaTween.Create(element, 1).Stop();
        PotaTween.Create(element, 1).
            SetAlpha(1f, 0f).
            SetDuration(0.5f).
            SetEaseEquation(Ease.Equation.OutSine).
            Play(() =>
            {
                if (callback != null)
                    callback();

                element.SetActive(false);
            });
    }

    private IEnumerator ShowAnswers()
    {
        Button[] buttons;
        if (_currentQuestion.Type == QuestionType.TXT)
            buttons = Buttons;
        else if (_currentQuestion.Type == QuestionType.IMG)
            buttons = ImageButtons;
        else
            buttons = new Button[0];

        DisableButtons();

        for (int i = 0; i < buttons.Length; i++)
        {
            buttons[i].gameObject.SetActive(true);

            PotaTween.Create(buttons[i].gameObject).Stop();
            PotaTween.Create(buttons[i].gameObject).
            SetAlpha(0f, 1f).
            SetScale(new Vector3(0.8f, 0.8f, 1), Vector3.one).
            SetDuration(0.5f).
            SetEaseEquation(Ease.Equation.OutBack).
            Play();

            yield return new WaitForSeconds(0.2f);
        }

        EnableButtons();
    }

    private IEnumerator HideWrongAnswers()
    {
        Button[] buttons;
        if (_currentQuestion.Type == QuestionType.TXT)
            buttons = Buttons;
        else if (_currentQuestion.Type == QuestionType.IMG)
            buttons = ImageButtons;
        else
            buttons = new Button[0];

        for (int i = 0; i < buttons.Length; i++)
        {
            if (i != CorrectAnswer)
            {
                HideButton(i);

                yield return new WaitForSeconds(0.2f);
            }

        }
    }

    private IEnumerator AudioCallback(AudioClip clip, System.Action callback)
    {
        float elapsedTime = 0;

        while(elapsedTime < clip.length)
        {
            elapsedTime += Time.deltaTime;
            yield return null;
        }

        callback();
    }
}
