
defesa civil npc 6.png
size: 1024,512
format: RGBA8888
filter: Linear,Linear
repeat: none
P L
  rotate: false
  xy: 226, 132
  size: 24, 24
  orig: 24, 24
  offset: 0, 0
  index: -1
P R
  rotate: false
  xy: 668, 258
  size: 12, 16
  orig: 12, 16
  offset: 0, 0
  index: -1
arm L
  rotate: false
  xy: 510, 198
  size: 156, 312
  orig: 156, 312
  offset: 0, 0
  index: -1
arm R 1
  rotate: false
  xy: 2, 8
  size: 184, 148
  orig: 184, 148
  offset: 0, 0
  index: -1
arm R 2
  rotate: false
  xy: 668, 362
  size: 72, 148
  orig: 72, 148
  offset: 0, 0
  index: -1
body
  rotate: false
  xy: 2, 158
  size: 256, 352
  orig: 256, 352
  offset: 0, 0
  index: -1
body_white mask
  rotate: false
  xy: 188, 36
  size: 36, 120
  orig: 36, 120
  offset: 0, 0
  index: -1
eyes 1
  rotate: true
  xy: 946, 418
  size: 92, 44
  orig: 92, 44
  offset: 0, 0
  index: -1
eyes 2
  rotate: false
  xy: 848, 374
  size: 96, 36
  orig: 96, 36
  offset: 0, 0
  index: -1
eyes 3
  rotate: false
  xy: 848, 462
  size: 96, 48
  orig: 96, 48
  offset: 0, 0
  index: -1
eyes 4
  rotate: false
  xy: 848, 412
  size: 96, 48
  orig: 96, 48
  offset: 0, 0
  index: -1
hand R
  rotate: false
  xy: 742, 418
  size: 104, 92
  orig: 104, 92
  offset: 0, 0
  index: -1
hand R finger 1
  rotate: true
  xy: 742, 372
  size: 44, 104
  orig: 44, 104
  offset: 0, 0
  index: -1
hand R finger 2
  rotate: true
  xy: 188, 2
  size: 32, 36
  orig: 32, 36
  offset: 0, 0
  index: -1
head
  rotate: false
  xy: 260, 166
  size: 248, 344
  orig: 248, 344
  offset: 0, 0
  index: -1
head neck
  rotate: false
  xy: 668, 276
  size: 68, 84
  orig: 68, 84
  offset: 0, 0
  index: -1
head s L
  rotate: false
  xy: 510, 168
  size: 52, 28
  orig: 52, 28
  offset: 0, 0
  index: -1
head s R
  rotate: false
  xy: 946, 404
  size: 16, 12
  orig: 16, 12
  offset: 0, 0
  index: -1
